from st2common.runners.base_action import Action
from lib.common import MEME_MAP


class MemeAliasesAction(Action):
    def run(self):
        return True, sorted(MEME_MAP.keys())
