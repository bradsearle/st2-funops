from st2common.runners.base_action import Action
from lib.common import MEME_MAP


class MemeAction(Action):
    def run(self, alias):
        return MEME_MAP.get(alias, {})
